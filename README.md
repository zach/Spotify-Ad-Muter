# Spotify Ad Muter
  This is a Python script that automatically mutes Spotify ads and unmutes when the ads are finished.

# Requirements
1. [Python 3.7.7](https://www.python.org/downloads/release/python-377/) 
2. Windows operating system

# How To Install/Use?
    1. Download 'main.py' & 'install requirements.bat'.
    2. Double click the "install requirements.bat", let it run.
    3. Open a commmand prompt in same directory as 'main.py'.
    4. Type 'python main.py' in the command prompt and hit enter
    5. Enjoy! :D
 
# Why Did I Make This?
I decided to create this script because I couldn't afford Spotify Premium and I hate blocking ads. Yeah, I could have blocked ads, but honestly, ads don't bother me.

# How It Works
This script automatically mutes and unmutes Spotify when ads play. It does this by checking for specific application titles (Advertisement, Spotify Free, Spotify, etc).

# Is This Legal?
Recently (in 2019), [Spotify has been cracking down on users circumventing their ads](https://www.cnet.com/tech/mobile/spotify-explicity-bans-ad-blockers-and-will-kick-off-users-that-have-them/ "my source"), they changed their User Guideline to not allow "circumventing or blocking advertisements or creating or distributing tools designed to block advertisements;" ([ Spotify User Guidelines ](https://www.spotify.com/us/legal/user-guidelines/ "my source")(Section 4; Line 10)). This Python script simply mutes/unmutes the Spotify.exe process and doesn't circumvent/block advertisements. Advertisements still play, hence giving Spotify their money.

**Circumvent Advertisements At Your Own Risk, I am not responsible for YOUR actions.** :)

# TODO
    - Improve the ad title checking logic as much as possible (for efficiency and effectiveness). If I can.
      - Improve how to store ad titles (What is better than what I use now?).
      
# Known Issues
    - (occasionally) When an ad finishes, you hear the ad for a split second.
      - (occasionally) unmute cuts off a few seconds of the beginning of a song, depends on the song length and ad length, I think.

# Changelog
Added this changelog to keep tabs on things.

  **10/13/2022**
    
    - [added] ad title.
    - [added] improved the README
    - [fixed] fixed bugs in the ad titles (missing , and empty string) that caused muting/unmuting to act odd
            - This small fix may have improved the program drastically, it seems more responsive than before.
<br>
<details>
<summary><b>Privacy Enhancements</b></summary>
<br>
If you are a privacy minded individual that hates personalized ads and data sharing for marketing purposes, you can disable both in Spotify Account settings.

### Disable 'Sharing Data For Marketing Purposes'
    - Click your username in the Spotify application
    - Account
    - Login to your account
    - Edit Profile
    - Uncheck 'Share my registration data with Spotify's content providers for marketing purposes.'

### Disable 'Personal data for tailored ads'
    - Click your username in the Spotify application
    - Account
    - Login to your account
    - Privacy settings
    - Uncheck 'Process Facebook data' & Uncheck 'Process my personal data for tailored ads'
    
### Enable 'Block All Cookies' Setting
    - Click your username in the Spotify application
    - Settings
    - Click 'Show Advanced Settings'
    - Check the 'Privacy' checkbox
</details>

# CONTRIBUTE
You can contribute to this project by adding any ad title(s) that don't already exist or by improving the Python code in anyway possible. Thanks!